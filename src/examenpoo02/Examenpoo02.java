/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenpoo02;

/**
 *
 * @author Hector Ramirez
 */
public class Examenpoo02 {

    private int numDocente;
    private String nombre;
    private Bono hijos;
    private String domicilio;
    private int nivel;
    private float pagoHora;
    int horas;

    public Examenpoo02() {
    }

    public Examenpoo02(int numDocente, String nombre, String domicilio, int nivel, float pagoHora, int horas) {
        this.numDocente = numDocente;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.nivel = nivel;
        this.pagoHora = pagoHora;
        this.horas = horas;
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }
    
    public float calcularPago(){
        float pago=0.0f;
        switch(this.nivel){
            case 0: pago = (this.pagoHora * 1.3f)*this.horas; break;
            case 1: pago = (this.pagoHora * 1.5f)*this.horas; break;
            case 2: pago = (this.pagoHora * 2f)*this.horas; break;
        }
        return pago;
    }
    
    public float calcularImpuesto(){
        float impuesto=0.0f;
        impuesto = this.calcularPago() * 0.16f;
        return impuesto;
    }
    
    public float calcularBono(int numHijos){
        float bono=0.0f;
          if(numHijos>0 && numHijos<3) bono = this.calcularPago()*0.05f;
          if(numHijos>2 && numHijos<6) bono = this.calcularPago()*0.1f;
          if(numHijos>5) bono = this.calcularPago()*0.2f;
        return bono;
    }
    
    public float calcularPagoTotal(int numHijos){
        float total = 0.0f;
        total = this.calcularPago() - this.calcularImpuesto() + this.calcularBono(numHijos);
        return total;
    }
   
}
